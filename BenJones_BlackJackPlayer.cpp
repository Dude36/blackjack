#include "BenJones_BlackJackPlayer.hpp"
#include "Card.h"
#include "Hand.h"

BenJones_BlackjackPlayer::BenJones_BlackjackPlayer() {
    cardCount = 0;
}


int BenJones_BlackjackPlayer::cardCount = 0;
/**
 * This function is called when it's your turn and you need to
 * choose whether to hit (return true) or stand (return false).
 *
 * The hand parameter tells you what's in your hand. You can call
 * getSuit(), getRank(), getLowValue(), and getHighValue() on each of
 * the cards it contains.
 *
 * The dealerUpCard parameter works the same way, and tells you what the
 * dealer up card is.
 *
 * You must return a value from this function indicating whether you want
 * to hit or stand. Return true if you want to hit and false if you want to stand.
 */
bool BenJones_BlackjackPlayer::hit(vector<Card> hand, Card dealerUpCard) {
    int cardValue = 0;

    for (int i = 0; i < (int)hand.size(); i++) {
        cardValue += hand[i].getLowValue();
    }
/*
* Cards 2-6 have a value of +1.
* Cards 7-9 have no value.
* Cards worth 10 have a value of -1.
* Aces also have a value of -1.
*/

    switch (cardValue) {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
            return true;
            break;
        case 7:
        case 8:
        case 9:
        case 10:
            return true;
            break;
        case 11:
            if (cardCount > 2 || dealerUpCard.getHighValue() < 10) {
                return false;
            } else {
                return true;
            }
            break;
        case 12:
            if (cardCount > 1 || dealerUpCard.getHighValue() < 10) {
                return false;
            } else {
                return true;
            }
            break;
        case 13:
            if (cardCount > 0 || dealerUpCard.getHighValue() < 9) {
                return false;
            } else {
                return true;
            }
            break;
        case 14:
            if (cardCount > -1 || dealerUpCard.getHighValue() < 8) {
                return false;
            } else {
                return true;
            }
          break;
            case 15:
            if (cardCount > -2 || dealerUpCard.getHighValue() < 7) {
                return false;
            } else {
                return true;
            }
            break;
        case 16:
            if (cardCount > -3 || dealerUpCard.getHighValue() < 6) {
                return false;
            } else {
                return true;
            }
            break;
        case 17:
        case 18:
        case 19:
        case 20:
        case 21:
        default:
            return false;
    }
}

/**
 * This function is called when it's your turn and you need to
 * choose to double down or not. If you choose to double down, you
 * will double your initial bet and receive one more card, then your
 * turn is over. Typically you would only double down if you think you
 * have a really good chance of beating the dealer base on your own hand
 * and the dealer's visible card.
 *
 * The hand parameter tells you what's in your hand. You can call
 * getSuit(), getRank(), getLowValue(), and getHighValue() on each of
 * the cards it contains.
 *
 * The dealerUpCard parameter works the same way, and tells you what the
 * dealer up card is.
 *
 * You must return a value from this function indicating whether you want
 * to double down or not. Return true if you want to double down and false
 * if you do not.
 */
bool BenJones_BlackjackPlayer::doubleDown(vector<Card> hand, Card dealerUpCard) {
    int cardValue = 0;
    for (int i = 0; i < (int)hand.size(); i++) {
        cardValue += hand[i].getHighValue();
    }

    if (cardCount < -5 || cardValue < 10) {
        return true;
    } else {
        return false;
    }
}

/**
 * This function is called to determine what your initial bet will be.
 * The function must return an integer between the values of 10 and 100.
 * The lowest bet you can make is 10 and the highest is 100. If you are
 * thinking of cheating and entering a number that is not in that range,
 * know that the game code checks to make sure a valid value is returned
 * and if it is not, your initialBet will default to 10.
 */
int BenJones_BlackjackPlayer::initialBet() {
    return 10;
}

/**
 * This function is called when the round ends. It's parameters contain
 * all of the cards visibly played. This doesn't contain any of the
 * cards in the dealer hole, or burn cards. The most practical thing to
 * do with this information if to count cards. Granted, it's not the best
 * strategy, but it'll help.
 *
 * Just like in Vegas, you can only count the cards that were visible on
 * the table. You also can't change this information, so cheating isn't
 * possible.
 */
void BenJones_BlackjackPlayer::cardsPlayed(vector<Card> cards) {
    for (int i = 0; i < (int)cards.size(); ++i) {
        switch(cards[i].getHighValue()) {
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                cardCount++;
                break;
            case 7:
            case 8:
            case 9:
                break;
            case 10:
            case 11:
                cardCount--;
                break;
            default:
                break;
        }
    }
}
