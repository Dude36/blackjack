#include "Card.h"

/**
 * Represents a card from a standard deck. Comprised of a
 * 3-tuple (suit, rank, value).
 */
Card::Card(){}

/**
 * Constructor to explicitly set entire card state.
 */
Card::Card(Suit setSuit, Rank setRank) {
	suit = setSuit;
	rank = setRank;
	switch (rank)
	{
		case ACE: value=1; break;
		case TWO: value=2; break;
		case THREE: value=3; break;
		case FOUR: value=4; break;
		case FIVE: value=5; break;
		case SIX: value=6; break;
		case SEVEN: value=7; break;
		case EIGHT: value=8; break;
		case NINE: value=9; break;
		case TEN: value=10; break;
		case JACK: value=10; break;
		case QUEEN: value=10; break;
		case KING: value=10; break;
	}
}

/**
 * Render this Card object as a string.
 */
void Card::printCard(bool override) {
	string retval = "";

	// Don't print the Card to the screen if the PRINT_VERBOSE flag is false
	if (!(PRINT_VERBOSE || override))
		return;

	switch (suit) {
		case CLUB:
			retval += "C";
			break;
		case DIAMOND:
			retval += "D";
			break;
		case SPADE:
			retval += "S";
			break;
		case HEART:
			retval += "H";
			break;
	}

	switch (rank) {
		case ACE: retval+="A"; break;
		case TWO: retval+="2"; break;
		case THREE: retval+="3"; break;
		case FOUR: retval+="4"; break;
		case FIVE: retval+="5"; break;
		case SIX: retval+="6"; break;
		case SEVEN: retval+="7"; break;
		case EIGHT: retval+="8"; break;
		case NINE: retval+="9"; break;
		case TEN: retval+="10"; break;
		case JACK: retval+="J"; break;
		case QUEEN: retval+="Q"; break;
		case KING: retval+="K"; break;
		
	}
	
	cout<<retval;
}


/**
 * Returns the suit of this card.
 */
Suit Card::getSuit() {
	return suit;
}

/**
 * Returns the rank of this card.
 */
Rank Card::getRank() {
	return rank;
}

/**
 * Returns the low value of this card.
 */
int Card::getLowValue() {
	return value;
}

/**
 * Returns the high value of this card.
 */
int Card::getHighValue() {
	// If the card is an Ace, return 11
	if (value==1)
		return 11;
	else
		return value;
}

