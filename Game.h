#ifndef GAME_H
#define GAME_H
#include <vector>
#include "Card.h"
#include "Deck.h"
#include "Hand.h"
#include "Scoreboard.h"
#include "Constants.h"
#include "Dealer.h"

/**
 * A Game object represents a single game of Blackjack in an overall match (of
 * possibly many games). Games are instantiated by providing them with a
 * roster of players, including a Scoreboard object through which scores
 * can be accumulated. The play() function then kicks off the game, which
 * will proceed from start to finish and update the Scoreboard.
 */
class Game {

public:

	/**
	 * Main constructor to instantiate a Game of Blackjack. The two parameters
	 * are two objects containing the player roster: a Scoreboard, and a class
	 * list. This constructor will create a newly shuffled deck and deal hands
	 * to all players and the dealer.
	 * @param scoreboard A fully-populated Scoreboard object that contains
	 * the names of the players, in order.
	 * @param playerClassList[] An array of Strings, each of which is a
	 * fully-qualified package/class name of a class that implements the
	 * BlackjackPlayer interface.
	 */
    Game(Scoreboard *scoreboard, vector<BlackjackPlayer *> playerClassList);

	/**
     * Play an entire Game of Blackjack from start to finish. Hands should have
     * already been dealt before this function is called. When the function is
	 * completed, the Game's scoreboard object will have been updated.
     */
    void play();

	void print(string s);

	void print(int i);

	void println(string s="");

	/* returns the dealer upcard
	 */
	Card getDealerUpCard();

	private:

	Card dealerHole;
	Deck deck;
	vector<Hand> h;
	Hand dealerHand = Hand(new Dealer(),"Dealer");
	int currPlayer;
	Scoreboard *scoreboard;

	void printState();
};
#endif
