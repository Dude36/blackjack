#include "Hand.h"
#include "Card.h"
#include "Game.h"

/**
 * Instantiate a Hand object to be played by the BlackjackPlayer class, and
 * the player name, passed as arguments. This implements a strategy
 * pattern whereby the constructor accepts various strategies that
 * implement the BlackjackPlayer interface.
 */
Hand::Hand(BlackjackPlayer *blackjackPlayer, string setPlayerName) {
	player = blackjackPlayer;
	playerName = setPlayerName;
	cards.clear();
	status=PLAYING;
}

/**
 * Add (draw) a card to the hand.
 */
void Hand::addCard(Card c) {
	cards.push_back(c);
}

/**
 * Return the number of cards in the hand.
 */
int Hand::size() {
	return (int)cards.size();
}

/**
 * It's your turn: Will you hit or stand.
 * @return true for a hit, false for a stand
 */
bool Hand::hit(Game *game) {
	return player->hit(cards, game->getDealerUpCard());
}

/**
 * It's your turn: Will you double your bet. After doubling down,
 * a player's initial bet is doubled and the player takes one more
 * card (or hit).
 * @return true if a player wants to double down, false otherwise
 */
bool Hand::doubleDown(Game *game) {
	if (player->doubleDown(cards, game->getDealerUpCard()))
	{
		doubleBet();
		return true;
	}
	else
	{
		return false;
	}
}

/**
 * This is called after every game to tell the player all cards
 * played this round. This allows you to cout cards mor accurately
 */
void Hand::showPlayerCards() {
	player->cardsPlayed(cards);
}


/**
 * Prints this Hand. See Card::printCard() for
 * notes about how individual cards are rendered.
 */
void Hand::printHand(bool override) {
	// Don't print the Hand to the screen if the PRINT_VERBOSE flag is false
	if (!(PRINT_VERBOSE || override))
		return;

	int numCards= (int)cards.size();
	for (int i=0; i<numCards; i++) {
		cards[i].printCard(override);
		if (i<numCards-1) {
			cout<<", ";
		}
	}
	cout<<" -> "<<countCardsBest();
}

/**
 * Return the lowest value of this Hand (counts all aces low).
 */
int Hand::countCardsLow() {
	int total = 0;
	int numCards= (int)cards.size();
	int i=0;
	for (i=0; i<numCards; i++) {
		total += cards[i].getLowValue();
	}
	return total;
}

/**
 * Return the best value of this Hand (highest value that
 * is 21 or below).
 */
int Hand::countCardsBest() {
	int total = countCardsLow();
	int numCards= (int)cards.size();
	int i=0;
	for (i=0; i<numCards; i++) {
		if (cards[i].getRank()==ACE)
		{
			if ((total + 10) <= 21)
			{
				total+=10;
				if (total==21)
				{
					status=BLACKJACK;
					return total;
				}
			}
		}
	}
	return total;
}

/**
 * Return the name of the player.
 */
string Hand::getPlayerName() {
	return playerName;
}

/**
 * get the status of the hand.
 */
Status Hand::getStatus() {
	return status;
}

/**
 * set the status of the hand.
 */
void Hand::setStatus(Status setStatus) {
	status=setStatus;
}

/**
 * It's your turn: What will your initial bet be. The
 * bet must be between 10 and 100. If the bet is out of these
 * bounds default the bet to 10.
 */
int Hand::initialBet(Game *game) {
	betAmount=player->initialBet();
	if (betAmount<10 || betAmount>100)
	{
		game->print(betAmount);
		game->println();
		game->println("Initial bet amount is invalid!");
		game->print("Defaulting to an initial bet of ");
		betAmount=10;
	}
	return betAmount;
}

int Hand::getBet() {
	return betAmount;
}

void Hand::setBet(int setBet) {
	betAmount=setBet;
}


void Hand::doubleBet() {
	betAmount*=2;
}

// Returns the card from the hand at the given index
Card Hand::getCard(int index)
{
	return cards[index];
}


