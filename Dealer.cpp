#include "Dealer.h"
#include "Card.h"
#include "Hand.h"

Dealer::Dealer() {
}

bool Dealer::hit(vector<Card> hand, Card dealerUpCard) {
	// This Dealer will hit if their best possible score is <17
	int bestScore=0;
	
	int numCards= (int)hand.size();
	int i=0;
	for (i=0; i<numCards; i++) {
		bestScore += hand[i].getLowValue();
	}
	
	for (i=0; i<numCards; i++) {
		if (hand[i].getRank()==ACE && (bestScore+10)<=21)
			bestScore += 10;
	}
	
	if (bestScore<17)
		return true;
	else
		return false;
}

bool Dealer::doubleDown(vector<Card> hand, Card dealerUpCard) {
	// The dealer will never double down
	return false;
}

int Dealer::initialBet() {
	// The dealer will not bet
	return 0;
}