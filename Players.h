#ifndef PLAYERS_H
#define PLAYERS_H
#include "BlackjackPlayer.h"

#include "DumbPlayer.h"
#include "JoshuaHigham_BlackjackPlayer.h"
// Include all Other Player class info.
#include "CalebDayley_blackjackplayer.hpp"
#include "CreedYorgason_BlackjackPlayer.h"
#include "Walker_BlackjackPlayer.h"
#include "MichaelHall_BlackjackPlayer.hpp"
#include "BenJones_BlackJackPlayer.hpp"

#endif // PLAYERS_H
