#ifndef CARD_H
#define CARD_H
#include <iostream>
#include <string>
#include "Constants.h"

using namespace std;

/**
 * Represents a card from a standard deck. Comprised of a 
 * 3-tuple (suit, rank, value).
 */
class Card {


public:
	/**
	 * Default Constructor.
	 */
	Card();
	
	/**
     * Constructor to explicitly set entire card state.
     */
	Card(Suit setSuit, Rank setRank);

    /**
     * Render this Card object as a string. 
	 */
    void printCard(bool override=false);

    /**
     * Returns the suit of this card
	 */
	Suit getSuit();

    /**
     * Returns the rank of this card.
     */
	Rank getRank();

    /**
     * Returns the low value of this card.
     */
    int getLowValue();
	
	/**
	 * Returns the high value of this card.
	 */
	int getHighValue();

private:
	Suit suit;
    Rank rank;
    int value;

};
#endif
