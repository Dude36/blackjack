#ifndef CONSTANTS_H
#define CONSTANTS_H
enum Suit { CLUB, DIAMOND, HEART, SPADE};
enum Rank { ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING };
enum Status {PLAYING, STAND, BUST, BLACKJACK};

	// An error message
	const int EMPTY_DECK = 10;

	// If PRINT_VERBOSE is true the game will output a play by play
	const bool PRINT_VERBOSE = true;

#endif
