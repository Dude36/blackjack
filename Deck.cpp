#include <algorithm>
#include "Deck.h"
#include "Card.h"


/**
 * Constructor for a new, full, shuffled Deck.
 */
Deck::Deck() {
	fillDeck();
	shuffle();
}

/**
 * Fill the card deck with the standard 52 cards.
 */
void Deck::fillDeck() {
	for (int i=CLUB; i<=SPADE; i++) {
		for (int j=ACE; j<=KING; j++) {
			cards.push_back(Card((Suit)i,(Rank)j));
		}
	}
}

/**
 * When performed on a Deck that is at least partially filled, the
 * cards are shuffled randomly. Note that this does not fill the Deck,
 * nor does it combine any outstanding cards into the Deck.
 */
void Deck::shuffle() {
	int numCards= (int)cards.size();
	std::random_shuffle(cards.begin(), cards.end());
}

/**
 * Returns true only if the Deck currently has no cards.
 */
bool Deck::isEmpty() {
	return cards.size() == 0;
}

/**
 * Returns the top card of the Deck, and removes it from the deck.
 * Adds the drawn card to the list of played cards.
 * throws EMPTY_DECK When called on an empty deck.
 */
Card Deck::draw() {
	if (cards.size() == 0) {
		throw EMPTY_DECK;
	}
	Card returnCard = cards[cards.size()-1];
	cards.pop_back();

	playedCards.push_back(returnCard);

	return returnCard;
}


/**
 * Remixes the deck by adding all previously played cards back into
 * the deck, and shuffling.
 */
void Deck::remix() {
	cards.insert(cards.end(), playedCards.begin(), playedCards.end());
	playedCards.clear();
	shuffle();
}

/**
 * Returns the list of played Cards since the last remix.
 */
vector<Card> Deck::getPlayedCards() {
	return playedCards;
}

/**
 * Prints the cards in the Deck.
 */
void Deck::printDeck() {

	int numCards= (int)cards.size();
	for (int i=0; i<numCards; i++) {
		cards[i].printCard(true);
		if (i<numCards-1) {
		   cout<<",";
		}
	}
}
