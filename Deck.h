#ifndef DECK_H  
#define DECK_H  
#include <vector>
#include <cstdlib>
#include <iostream>

using namespace std;

class Card;

/**
 * A Deck of cards, consisting of both a draw and discard pile. The
 * functions draw() and discard() act in concert to (a) remove and return the
 * top card of the Deck, and (b) recycle a previously drawn Card to the
 * discard pile. remix() puts all discarded cards back in play and
 * reshuffles the deck.
 *
 */
class Deck {

public:

    /**
     * In a shuffle, the number of deck-sizes that two cards in random
     * positions will be swapped. (Empirically, setting this to 1 gives a
     * good shuffle, but theoretically increasing this will lead to more
     * thoroughly shuffled decks.)
     */
    static const int SHUFFLE_FACTOR = 1;

    
    /**
     * Constructor for a new, full, shuffled Deck.
     */
    Deck();

	/**
	 * Fill the card deck with the standard 52 cards.
	 */
	void fillDeck();

    /**
     * When performed on a Deck that is at least partially filled,
     * shuffles its cards randomly. Note that this does not fill the Deck,
     * nor does it combine any outstanding cards into the Deck.
     */
    void shuffle();

    /**
     * Returns true only if the Deck currently has no cards.
     */
    bool isEmpty();

    /**
     * Returns the top card of the Deck, and removes it.
     * @throws EmptyDeckException When called on an empty deck.
     * @see isEmpty
     */
    Card draw();

    /**
     * Remixes the deck by adding all previously played cards back into
     * the deck, and shuffling.
     */
    void remix();

    /**
     * Returns the list of played Cards since the last remix.
     */
    vector<Card> getPlayedCards();

	/**
	 * Prints the cards in the Deck.
	 */
	void printDeck();

private:
	vector<Card> cards;
    vector<Card> playedCards;
};
#endif
