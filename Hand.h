#ifndef HAND_H
#define HAND_H
#include <vector>
#include <iostream>
#include "BlackjackPlayer.h"

using namespace std;

class Card;
class Game;

/**
 * A Hand of cards, held by a particular player. A Hand object is
 * responsible for playing a Card (i.e., actually choosing a card to
 * play) when the player's turn comes up. To do this, it implements the
 * strategy pattern by which this choice can be delegated to an arbitrary
 * implementer of the UnoPlayer class.
 */
class Hand {

public:

	/**
	 * Instantiate a Hand object to be played by the BlackjackPlayer class, and
	 * the player name, passed as arguments. This implements a strategy
	 * pattern whereby the constructor accepts various strategies that
	 * implement the BlackjackPlayer interface.
	 */
    Hand(BlackjackPlayer *blackjackPlayer, string setPlayerName);

    /**
     * Add (draw) a card to the hand.
     */
    void addCard(Card c);

    /**
     * Return the number of cards in the hand.
     */
    int size();

	/**
	 * It's your turn: Will you hit or stand.
	 * @return true for a hit, false for a stand
	 */
    bool hit(Game *game);


    /**
     * Prints this Hand. See Card::printCard() for
     * notes about how individual cards are rendered.
     */
    void printHand(bool override=false);

    /**
     * Return the value of this Hand, as it now stands.
     */
    int countCardsLow();

	/**
	 * Return the value of this Hand, as it now stands.
	 */
	int countCardsBest();

    /**
     * Return the name of the player.
     */
    string getPlayerName();

	/**
	 * get the status of the hand.
	 */
	Status getStatus();

	/**
	 * set the status of the hand.
	 */
	void setStatus(Status setStatus);

	int getBet();

	void setBet(int setBet);

	void doubleBet();

	int initialBet(Game *game);

	/**
	 * It's your turn: Will you double your bet. After doubling down,
	 * a player's initial bet is doubled and the player takes one more
	 * card (or hit).
	 * @return true if a player wants to double down, false otherwise
	 */
	bool doubleDown(Game *game);

    /**
     * This is called after every game to tell the player all cards
     * played this round. This allows you to cout cards mor accurately
     */
    void showPlayerCards();

	// Returns the card from the hand at the given index
	Card getCard(int index);

private:
	vector<Card> cards;
    BlackjackPlayer *player;
    string playerName;
	Status status;
	int betAmount;
};
#endif
