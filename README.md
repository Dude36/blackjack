# README #

# Blackjack

This is a simple demonstration of using inheritance to create many different players off on one parent class.

### What to Implement ###

Take one of the sample players (JoshuaHigham or DumbPlayer) and overwrite the following with your own stuff.

* initializer

Basic class initializer.

* hit()

This is where you will decide whether to hit or stay based on whatever parameters you may have inside your class.

* doubleDown()

Do you consider yourself a high roller? Then this function is meant for you. This is a boolean function to determine if you want to double your bet against the dealer and other players.

* initalBet()

How much to bet at the start of the round.

* cardsPlayed()

Passes all cards played as a parameter to supplement counting cards.

### Counting Cards ###

Card counting is actually easier than it's made out to be in the movies. You just need to follow these simple rules:

* If the dealer pulls a card that's less than 7, add 1 to your count
* if the dealer pulls a card between 7 and 9, don't change your count
* if the dealer pulls a card that's greater than 9, subtract 1 to your count

Therefore, if our count is greater than 0, you know there are more high cards in the deck, than low cards. The inverse also applies.

for more information, check out the [Card Counting](https://en.wikipedia.org/wiki/Card_counting) Wikipedia page.
