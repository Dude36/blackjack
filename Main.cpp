#include "BlackjackPlayer.h"
#include "Constants.h"
#include "Game.h"
#include "Players.h"

#include <algorithm>
#include <ctime>
#include <iostream>
#include <map>
#include <string>

using namespace std;

static vector<string> playerNames;
static vector<BlackjackPlayer *> playerClasses;

const string ARG_GAME_LOOPS		= "-g";

map<string, BlackjackPlayer*> populateMap();

int main(int argc, const char* argv[])
{
	// Seed the random number generator which is used to
	// shuffle the deck.
	srand((unsigned)time(NULL));

	// This indicates how many games to play. Start with
	// 1 game and get your code working properly before
	// increasing the number of games.
	int numGames = 1000;

	// Command Line arguments to change the ability to
	// get info for adding the player
	if (argc > 1) {
		for (int i = 1; i < argc; ++i) {
			string argument(argv[i]);
			if (argument == ARG_GAME_LOOPS) {
				cout << argv[++i] << " Games requested." << endl;
				numGames = atoi(argv[i]);
			}
			// else if (argument == "-v" ) { PRINT_VERBOSE = true; }
		}
	}

	std::map<string, BlackjackPlayer*> players = populateMap();
	for (auto const a : players) {
		playerNames.push_back(a.first);
		playerClasses.push_back(a.second);
	}

	// This code creates the scoreboard and runs the games. You
	// should not change the code below.
	Scoreboard s = Scoreboard(playerNames);
	for (int i=0; i<numGames; i++) {
		Game g = Game(&s,playerClasses);
		g.play();
	}

	cout<<"\nScore after "<<numGames<<" Game(s):\n";
	s.printScoreboard();

	return 0;
}

map<string, BlackjackPlayer*> populateMap() {
    std::map<string, BlackjackPlayer*> map;
    map["Dummy Player"] = new DumbPlayer();
    map["Joshua Higham"] = new JoshuaHigham_BlackjackPlayer();
    // Add Players Here
    map["Caleb Dayley"] = new CalebDayley_BlackjackPlayer();
    map["Creed Yorgason"] = new CreedYorgason_BlackjackPlayer();
	map["Michael Hall"] = new MichaelHall_blackjackPlayer();
	map["Walker"] = new Walker_BlackjackPlayer();
	map["Ben Jones"] = new BenJones_BlackjackPlayer();
    return map;
}
