#include "Game.h"
#include "Hand.h"
#include "BlackjackPlayer.h"

/**
 * Main constructor to instantiate a Game of Blackjack. The two parameters
 * are two objects containing the player roster: a Scoreboard, and a class
 * list. This constructor will create a newly shuffled deck and deal hands
 * to all players and the dealer.
 * @param scoreboard A fully-populated Scoreboard object that contains
 * the names of the players, in order.
 * @param playerClassList[] An array of Strings, each of which is a
 * fully-qualified package/class name of a class that implements the
 * BlackjackPlayer interface.
 */
Game::Game(Scoreboard *setScoreboard, vector<BlackjackPlayer *> playerClassList) {
	scoreboard = setScoreboard;
	deck = Deck();
	currPlayer = 0;
	try {
		// add players to the game
		for (int i=0; i<scoreboard->getNumPlayers(); i++) {
			h.push_back(Hand(playerClassList[i],
							 scoreboard->getPlayerList()[i]));
		}

		// Deal first card
		for (int i=0; i<scoreboard->getNumPlayers(); i++) {
			h[i].addCard(deck.draw());
		}
		dealerHole=deck.draw();

		// Deal second card
		for (int i=0; i<scoreboard->getNumPlayers(); i++) {
			h[i].addCard(deck.draw());
		}
		dealerHand.addCard(deck.draw());
	}
	catch (int i) {
		// This shouldn't happen unless the number of players is too high (>12)
		if(i==EMPTY_DECK)
		{
			print("Can't deal initial hands!\n");
		}
	}
}

void Game::printState() {
	// Don't print the State to the screen if the PRINT_VERBOSE flag is false
	if (!PRINT_VERBOSE)
		return;

   cout<<"Dealer Hand: ";
   dealerHand.printHand();
   cout<<endl;

	for (int i=0; i<scoreboard->getNumPlayers(); i++) {
		cout<<h[i].getPlayerName()<<" Hand: ";
		h[i].printHand();
		cout<<endl;
	}
}

/**
 * Play an entire Game of Blackjack from start to finish. Hands should have
 * already been dealt before this function is called. When the function is
 * completed, the Game's scoreboard object will have been updated.
 */
void Game::play() {
	println("\n\n======NEW GAME - INITIAL DEAL======");
	printState();
	println();

	// Give each player a turn to resolve their hand
	for (int i=0; i<scoreboard->getNumPlayers(); i++) {
		print("-------");
		print(h[i].getPlayerName());
		println(" Turn------");

		// get initial bet
		print("Initial Bet: ");
		print(h[i].initialBet(this));
		println();

		// Test to see if the hand is a blackjack.
		if (h[i].countCardsBest()==21)
		{
			println("BLACKJACK!");
			h[i].setStatus(BLACKJACK);
		}

		while (h[i].getStatus()==PLAYING)
		{

			// First determine if the player wants to double down, but
			// only offer this option if the player is playing on their
			// initial two-card hand.
			if (h[i].size()==2 && h[i].doubleDown(this))
			{
				// The player has doubled down. The initial bet gets
				// doubled in the doubleDown() function for the current
				// hand. Now we draw one card and then stand.
				print("Double Down: ");
				h[i].addCard(deck.draw());
				h[i].setStatus(STAND);
			}
			else
			{
			    // See if the player want to hit.
				if (h[i].hit(this))
				{
					print("Hit: ");
					h[i].addCard(deck.draw());
				}
				else
				{
					print("Stand: ");
					h[i].setStatus(STAND);
				}
			}

			// display current hand
			h[i].printHand();
			println();

			// Check to see if the hand has busted
			if (h[i].countCardsBest()>21)
			{
				println("BUST!");
				h[i].setStatus(BUST);
				continue;
			}

		}
		println();
	}

	// Take the Dealer's turn
	println("-------Dealer Turn------");

	// Show the card in the hole that has been hidden
	// up until this point
	print("Card in the Hole: ");
	dealerHole.printCard();
	println();

	// Add the hole card to the dealer's hand
	dealerHand.addCard(dealerHole);

	// Test to see if the dealer has a Blackjack.
	if (dealerHand.countCardsBest()==21)
	{
		print("BlackJack: ");
		dealerHand.printHand();
		println();
		dealerHand.setStatus(BLACKJACK);
	}

	while (dealerHand.getStatus()==PLAYING)
	{
		// See if the dealer wants to hit (or stand). The logic for this
		// decision is contained in the Dealer class
		if (dealerHand.hit(this))
		{
			print("Hit: ");
			dealerHand.addCard(deck.draw());
		}
		else
		{
			print("Stand: ");
			dealerHand.setStatus(STAND);
		}

		dealerHand.printHand();
		println();

		// check to see if the dealer hand has busted
		if (dealerHand.countCardsBest()>21)
		{
			println("BUST!");
			dealerHand.setStatus(BUST);
		}
	}

	println("\n-------Results------");

	// Score this hand
	if (dealerHand.getStatus()==BUST)
	{
		println("Dealer: BUST");
		// If the dealer went Bust then all players still in the game win
		for (int i=0; i<scoreboard->getNumPlayers(); i++) {
			print(h[i].getPlayerName());
			print(": ");

			// Players with a Blackjack get 1.5X their initial bet.
			// Players that standed get 1X their initial bet.
			// Players that busted lose their initial bet.
			if (h[i].getStatus()==BLACKJACK)
			{
				println("BLACKJACK");
				print("   Score: ");
				print(h[i].getBet()*1.5);
				println();
				scoreboard->addToScore(i,h[i].getBet()*1.5);
			}
			else if (h[i].getStatus()==STAND)
			{
				print(h[i].countCardsBest());
				println();
				print("   Score: ");
				print(h[i].getBet());
				println();
				scoreboard->addToScore(i,h[i].getBet());
			}
			else
			{
				println("BUST");
				print("   Score: ");
				print(-h[i].getBet());
				println();
				scoreboard->addToScore(i,-h[i].getBet());
			}
		}
	} else if (dealerHand.getStatus()==BLACKJACK)
	{
		println("Dealer: BLACKJACK");

		// If the dealer had a Blackjack then other players with a Blackjack make a push.
		// All other player lose their bet.
		for (int i=0; i<scoreboard->getNumPlayers(); i++) {
			print(h[i].getPlayerName());
			print(": ");

			if (h[i].getStatus()==BLACKJACK)
			{
				println("BLACKJACK");
				println("   Score: Push=0");
			}
			else if (h[i].getStatus()==STAND)
			{
				print(h[i].countCardsBest());
				println();
				print("   Score: ");
				print(-h[i].getBet());
				println();
				scoreboard->addToScore(i,-h[i].getBet());
			}
			else
			{
				println("BUST");
				print("   Score: ");
				print(-h[i].getBet());
				println();
				scoreboard->addToScore(i,-h[i].getBet());
			}
		}
	} else if (dealerHand.getStatus()==STAND)
	{
		print("Dealer: ");
		print(dealerHand.countCardsBest());
		println();

		// If the dealer standed then players with a Blackjack get 1.5X their
		// initial bet. Players that standed and had a higher
		// score (but no bust) get 1X their initial bet. Players
		// that tied with the dealer make a push. Players that score less than the
		// dealer lose their bet. Finally, players that bust lose their bet.
		for (int i=0; i<scoreboard->getNumPlayers(); i++) {
			print(h[i].getPlayerName());
			print(": ");

			if (h[i].getStatus()==BLACKJACK)
			{
				println("BLACKJACK");
				print("   Score: ");
				print(h[i].getBet()*1.5);
				println();
				scoreboard->addToScore(i,h[i].getBet()*1.5);
			}
			else if (h[i].getStatus()==STAND)
			{
				if (dealerHand.countCardsBest()<h[i].countCardsBest())
				{
					print(h[i].countCardsBest());
					println();
					print("   Score: ");
					print(h[i].getBet());
					println();
					scoreboard->addToScore(i,h[i].getBet());
				}
				else if (dealerHand.countCardsBest()>h[i].countCardsBest())
				{
					print(h[i].countCardsBest());
					println();
					print("   Score: ");
					print(-h[i].getBet());
					println();
					scoreboard->addToScore(i,-h[i].getBet());
				} else
				{
					print(h[i].countCardsBest());
					println();
					println("   Score: Push=0");
				}
			}
			else
			{
				println("BUST");
				print("   Score: ");
				print(-h[i].getBet());
				println();
				scoreboard->addToScore(i,-h[i].getBet());
			}
		}
	}
	println("\n");

	// Send played card Information to all the players.
	for (int i = 0; i < scoreboard->getNumPlayers(); ++i) {
		h[i].showPlayerCards();
	}
}

void Game::print(string s) {
	if (PRINT_VERBOSE) {
		cout<<s;
	}
}

void Game::print(int i) {
	if (PRINT_VERBOSE) {
		cout<<i;
	}
}


void Game::println(string s) {
	if (PRINT_VERBOSE) {
		cout<<s<<endl;
	}
}

/* returns the dealer upcard
 */
Card Game::getDealerUpCard() {
	return dealerHand.getCard(0);
}


