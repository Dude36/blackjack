#ifndef Dealer_H
#define Dealer_H
#include "BlackjackPlayer.h"

class Dealer: public BlackjackPlayer {
	
public:
	Dealer();   // default constructor
	
	bool hit(vector<Card> hand, Card dealerUpCard);
	
	bool doubleDown(vector<Card> hand, Card dealerUpCard);
	
	int initialBet();
};
#endif